
terraform {
  required_providers {
    aws = {
      source  = "aws"
      version = "~> 3.37.0"
    }
  }
}

provider "aws" {
  region  = "eu-central-1"
}


data "aws_route53_zone" "root" {
  name = var.main_domain
}

resource "aws_route53_zone" "subdomain" {
  name = "${var.sub_domain}.${var.main_domain}"
}


variable "main_domain" {
  type        = string
  description = "primary domain name"
  default     = "aws.tikal.io."
}

variable "sub_domain" {
  type        = string
  description = "Domain prefix"
  default     = "ig"
}


resource "aws_route53_record" "subdomain-ns" {
  zone_id = "Z04435812BP8Y3Q7UVJ99"
  name    = "${var.sub_domain}.${var.main_domain}"
  type    = "NS"
  ttl     = "30"
  records = aws_route53_zone.subdomain.name_servers
  allow_overwrite = true
}

resource "aws_route53_record" "bastion_a_record" {
  zone_id = aws_route53_zone.subdomain.zone_id
  name    = "bastion.${var.sub_domain}.${var.main_domain}"
  type    = "A"
  ttl     = "30"
  records = [aws_eip.bastion_ip.public_ip]
  allow_overwrite = true
}



resource "aws_iam_policy" "r53_policy" {
  name        = "route53_policy"
  path        = "/"
  description = "Allow ec2 instance to access dns records and zons"

  policy = "${file("aws_policies/route53_role.json")}"
}



resource "aws_iam_role" "bastion_role" {
  name = "bastion_access"
  assume_role_policy = "${file("aws_policies/asu_role.json")}"
}



resource "aws_iam_instance_profile" "bastion_profile" {
  name = "bastion_access"
  role = aws_iam_role.bastion_role.name
}



resource "aws_iam_policy_attachment" "policy_attach" {
  name       = "policy_attach"
  roles      = ["${aws_iam_role.bastion_role.name}"]
  policy_arn = "${aws_iam_policy.r53_policy.arn}"
}


# create ec2 instance
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "bastion_server" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.medium"
  iam_instance_profile = aws_iam_instance_profile.bastion_profile.name
  security_groups = [aws_security_group.bastion_ssh.name]

  key_name = aws_key_pair.bastion_key.key_name

  tags = {
    Name = "Bastion"
  }
}

resource "aws_key_pair" "bastion_key" {
  key_name   = "bastion-key"
  public_key = "${file("~/.ssh/bastion.pub")}"
}



resource "aws_security_group" "bastion_ssh" {
  name        = "bastion_ssh"
  description = "Allow SSH inbound traffic to Bastion server"

  ingress {
    description      = "SSH to Bastion"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_ssh"
  }
}


resource "aws_eip" "bastion_ip" {
  instance = aws_instance.bastion_server.id
}


