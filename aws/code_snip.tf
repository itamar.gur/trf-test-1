
# terraform {
#   required_providers {
#     aws = {
#       source  = "aws"
#       version = "~> 3.37.0"
#     }
#   }
# }

# provider "aws" {
#   region  = "eu-central-1"
# }


# resource "aws_route53_zone" "root" {
#   name = var.main_domain
#   force_destroy = "false"
# }

# resource "aws_route53_zone" "subdomain" {
#   name = "${var.sub_domain}.${var.main_domain}"
# }


# variable "main_domain" {
#   type        = string
#   description = "primary domain name"
#   default     = "aws.tikal.io."
# }

# variable "sub_domain" {
#   type        = string
#   description = "Domain prefix"
#   default     = "ig"
# }

# resource "aws_route53_record" "subdomain-ns" {
#   zone_id = aws_route53_zone.root.zone_id
#   name    = "${var.sub_domain}.${var.main_domain}"
#   type    = "NS"
#   ttl     = "30"
#   records = aws_route53_zone.subdomain.name_servers
#   allow_overwrite = true
# }




# # IAM role
# resource "aws_iam_policy" "r53_policy" {
#   name        = "route53_policy"
#   path        = "/"
#   description = "Allow ec2 instance to access dns records and zons"

#   # Terraform's "jsonencode" function converts a
#   # Terraform expression result to valid JSON syntax.
#   policy = jsonencode({
#     Version = "2012-10-17"
#     Statement = [
#       {
#         Action = [
#           "route53:*",
#         ]
#         Effect   = "Allow"
#         Resource = "*"
#       },
#     ]
#   })
# }


# resource "aws_iam_role" "role" {
#   name = "test_role"
#   path = "/"

#   assume_role_policy = aws_iam_policy.r53_policy.policy
# }

# resource "aws_iam_instance_profile" "test_profile" {
#   name = "test_profile"
#   role = aws_iam_role.role.name
# }

# # create ec2 instance
# data "aws_ami" "ubuntu" {
#   most_recent = true

#   filter {
#     name   = "name"
#     values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
#   }

#   filter {
#     name   = "virtualization-type"
#     values = ["hvm"]
#   }

#   owners = ["099720109477"] # Canonical
# }

# resource "aws_instance" "web" {
#   ami           = data.aws_ami.ubuntu.id
#   instance_type = "t2.medium"
#   iam_instance_profile = aws_iam_instance_profile.test_profile.nameß

#   tags = {
#     Name = "HelloWorld"
#   }
# }




