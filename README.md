Pre requierments: 1. Install Terraform
2. Install AWS-CLI
3. Configure defualt AWS access key on defualt profile (`aws sts get-caller-identity`)
4. Get existing base zone id for `aws.tikal.io`
5. Set up Git repository to store solution




 # Task definition:

-[ ] create public sub-domain `<username>.aws.tikal.io`


 # Definition of done

 `<username>.aws.tikal.io` answers to DNS Lookup:

```sh
dig <username>.aws.tikal.io -t ns
```

example of valid resualt:

```sh

...
;; ANSWER SECTION:
aws.tikal.io.           172800  IN      NS      ns-1109.awsdns-10.org.
aws.tikal.io.           172800  IN      NS      ns-175.awsdns-21.com.
aws.tikal.io.           172800  IN      NS      ns-1959.awsdns-52.co.uk.
aws.tikal.io.           172800  IN      NS      ns-720.awsdns-26.net.

...
```
